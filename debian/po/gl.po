# Galician translation of mdadm's debconf templates
# This file is distributed under the same license as the mdadm package.
# Jacobo Tarrio <jtarrio@debian.org>, 2007, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: mdadm\n"
"Report-Msgid-Bugs-To: mdadm@packages.debian.org\n"
"POT-Creation-Date: 2019-02-09 08:48+0100\n"
"PO-Revision-Date: 2008-02-06 23:45+0000\n"
"Last-Translator: Jacobo Tarrio <jtarrio@debian.org>\n"
"Language-Team: Galician <proxecto@trasno.net>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../mdadm.templates:2001
msgid "Should mdadm run monthly redundancy checks of the MD arrays?"
msgstr ""
"¿Debería mdadm facer comprobacións mensuais de redundancia dos arrays MD?"

#. Type: boolean
#. Description
#: ../mdadm.templates:2001
msgid ""
"If the kernel supports it (versions greater than 2.6.14), mdadm can "
"periodically check the redundancy of MD arrays (RAIDs). This may be a "
"resource-intensive process, depending on the local setup, but it could help "
"prevent rare cases of data loss. Note that this is a read-only check unless "
"errors are found; if errors are found, mdadm will try to correct them, which "
"may result in write access to the media."
msgstr ""
"Se o núcleo ten soporte para iso (en versións superiores á 2.6.14), mdadm "
"pode facer comprobacións periódicas de redundancia dos arrays MD (RAIDs). "
"Este pode ser un proceso intensivo en recursos, dependendo da configuración "
"local, pero pode axudar a evitar algúns casos raros de perdas de datos. Teña "
"en conta que esta é unha comprobación de só lectura a menos que se atopen "
"erros; se se atopan erros, mdadm ha tratar de os arranxar, o que pode "
"producir accesos de escritura aos soportes."

#. Type: boolean
#. Description
#: ../mdadm.templates:2001
msgid ""
"The default, if turned on, is to check on the first Sunday of every month at "
"01:06."
msgstr ""
"A opción por defecto, se se activa, é facer as comprobacións o primeiro "
"domingo de cada mes ás 01:16."

#. Type: boolean
#. Description
#: ../mdadm.templates:3001
msgid "Should mdadm check once a day for degraded arrays?"
msgstr ""

#. Type: boolean
#. Description
#: ../mdadm.templates:3001
msgid ""
"mdadm can check once a day for degraded arrays and missing spares to ensure "
"that such events don't go unnoticed."
msgstr ""

#. Type: boolean
#. Description
#: ../mdadm.templates:4001
msgid "Do you want to start the MD monitoring daemon?"
msgstr "¿Quere iniciar o servizo de monitorización de MD?"

#. Type: boolean
#. Description
#: ../mdadm.templates:4001
msgid ""
"The MD (RAID) monitor daemon sends email notifications in response to "
"important MD events (such as a disk failure)."
msgstr ""
"O servizo de monitorización de MD (RAID) envía avisos por email en resposta "
"a eventos importantes de MD (coma fallos nos discos)."

#. Type: boolean
#. Description
#: ../mdadm.templates:4001
msgid "Enabling this option is recommended."
msgstr "Recoméndase activar esta opción."

#. Type: string
#. Description
#: ../mdadm.templates:5001
msgid "Recipient for email notifications:"
msgstr "Destinatario para os avisos por email:"

#. Type: string
#. Description
#: ../mdadm.templates:5001
msgid ""
"Please enter the email address of the user who should get the email "
"notifications for important MD events."
msgstr ""
"Introduza o enderezo de email do usuario que debe recibir os avisos por "
"email de eventos importantes de MD."

#~ msgid "MD arrays needed for the root file system:"
#~ msgstr "Arrays MD necesarios para o sistema de ficheiros raíz"

#~ msgid ""
#~ "Please enter 'all', 'none', or a space-separated list of devices such as "
#~ "'md0 md1' or 'md/1 md/d0' (the leading '/dev/' can be omitted)."
#~ msgstr ""
#~ "Introduza \"all\" (todos), \"none\" (ningún) ou unha lista de "
#~ "dispositivos separados por espazos, tales coma \"md0 md1\" ou \"md/1 "
#~ "md/0\" (pódese omitir o \"/dev/\" do principio)."

#~ msgid "for internal use - only the long description is needed."
#~ msgstr "para uso interno - só se precisa da descrición longa."

#~ msgid ""
#~ "If the system's root file system is located on an MD array (RAID), it "
#~ "needs to be started early during the boot sequence. If it is located on a "
#~ "logical volume (LVM), which is on MD, all constituent arrays need to be "
#~ "started."
#~ msgstr ""
#~ "Se o sistema de ficheiros raíz do sistema está ubicado nun array MD "
#~ "(RAID), hai que o iniciar no principio da secuencia de inicio. Se está "
#~ "ubicado nun volume lóxico (LVM) que está nun MD, hai que iniciar os "
#~ "arrays constituíntes."

#~ msgid ""
#~ "If you know exactly which arrays are needed to bring up the root file "
#~ "system, and you want to postpone starting all other arrays to a later "
#~ "point in the boot sequence, enter the arrays to start here. "
#~ "Alternatively, enter 'all' to simply start all available arrays."
#~ msgstr ""
#~ "Se sabe exactamente que arrays son necesarios para erguer o sistema de "
#~ "ficheiros raíz, e se quere pospor o inicio dos demáis arrays ata un punto "
#~ "posterior da secuencia de inicio, introduza aquí os arrays a iniciar. "
#~ "Alternativamente, introduza \"all\" para iniciar tódolos arrays "
#~ "dispoñibles."

#~ msgid ""
#~ "If you do not need or want to start any arrays for the root file system, "
#~ "leave the answer blank (or enter 'none'). This may be the case if you are "
#~ "using kernel autostart or do not need any arrays to boot."
#~ msgstr ""
#~ "Se non quere ou precisa de iniciar ningún array para o sistema de "
#~ "ficheiros raíz, deixe a resposta en branco (ou introduza \"none\"). Este "
#~ "pode ser o caso se está a empregar o autoinicio do núcleo ou non precisa "
#~ "de ningún array para o inicio."

#~ msgid "An error occurred: device node does not exist"
#~ msgstr "Houbo un erro: o nodo do dispositivo non existe"

#~ msgid "An error occurred: not a block device"
#~ msgstr "Houbo un erro: non é un dispositivo de bloques"

#~ msgid "An error occurred: not an MD array"
#~ msgstr "Houbo un erro: non é un array MD"

#~ msgid "An error occurred: array not listed in mdadm.conf file"
#~ msgstr "Houbo un erro: o array non figura no ficheiro mdadm.conf"

#~ msgid "Start arrays not listed in mdadm.conf?"
#~ msgstr "¿Iniciar os arrays que non figuran en mdadm.conf?"

#~ msgid ""
#~ "The specified array (${array}) is not listed in the configuration file "
#~ "(${config}). Therefore, it cannot be started during boot, unless you "
#~ "correct the configuration file and recreate the initial ramdisk."
#~ msgstr ""
#~ "O array indicado (${array}) non figura no ficheiro de configuración "
#~ "(${config}). Polo tanto, non se pode arrincar no inicio do sistema, a "
#~ "menos que corrixa o ficheiro de configuración e volva crear o disco RAM "
#~ "inicial."

#~ msgid ""
#~ "This warning is only relevant if you need arrays to be started from the "
#~ "initial ramdisk to be able to boot. If you use kernel autostarting, or do "
#~ "not need any arrays to be started as early as the initial ramdisk is "
#~ "loaded, you can simply continue. Alternatively, choose not to continue "
#~ "and enter 'none' when prompted which arrays to start from the initial "
#~ "ramdisk."
#~ msgstr ""
#~ "Este aviso só é relevante se precisa de iniciar arrays desde o disco RAM "
#~ "inicial para poder iniciar o sistema. Se emprega autoinicio do núcleo ou "
#~ "non precisa de iniciar arrays tan pronto como se cargue o disco RAM "
#~ "inicial, pode continuar. De xeito alternativo, escolla non continuar e "
#~ "introduza \"none\" cando se lle pregunte que arrays quere iniciar do "
#~ "disco RAM inicial."
